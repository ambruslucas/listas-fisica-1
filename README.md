# IF/UFRJ - Física 1 - Turma Unificada

Arquivos das listas da disciplina de física 1.

OBS 1: Novos exercícios estarão sendo adicionados à uma 
lista enquanto não houver commit final do arquivo tex.

Para dúvidas, erros, sugestões ou agradecimentos:
Entre em contato através do meu email pessoal 
ambruslucas@gmail.com

OBS 2: Mensagens do IF serão ignoradas, da mesma
forma que o IF ignora as demandas dos estudantes da UFRJ.
